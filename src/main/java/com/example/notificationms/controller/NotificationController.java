package com.example.notificationms.controller;

import com.example.notificationms.dto.request.NotificationRequestDto;
import com.example.notificationms.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class NotificationController {
//    @GetMapping
//    public String print(@RequestParam(value = "orderId", required = false) String orderId){
//        System.out.println("order id = " + orderId);
//        return "notification send successfully";
//    }

    private final NotificationService notificationService;

    @PostMapping
    public void sendNotification(@RequestBody NotificationRequestDto notificationRequestDto,
                                 @RequestHeader("X-USER-ID") UUID userId,
                                 @RequestHeader("X-USER-EMAIL") String userEmail) {
        notificationService.sendNotification(notificationRequestDto, userId, userEmail);
    }
}
