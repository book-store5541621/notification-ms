package com.example.notificationms.dto.request;

import java.util.UUID;

public record NotificationRequestDto(String message, UUID orderId, UUID userId, String userEmail) {
}
