package com.example.notificationms.dto.response;

import java.util.UUID;

public record OrderResponseDto(UUID bookId, int quantity, UUID userId) {
}