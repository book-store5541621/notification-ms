package com.example.notificationms.dto.response;

import java.util.UUID;

public record NotificationResponseDto(String message, UUID orderId, UUID userId, String userEmail) {
}
