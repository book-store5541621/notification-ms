package com.example.notificationms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
public class NotificationMsApplication {
    public static void main(String[] args) {
        SpringApplication.run(NotificationMsApplication.class, args);
    }
}
