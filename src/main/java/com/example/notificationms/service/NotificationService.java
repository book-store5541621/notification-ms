package com.example.notificationms.service;

import com.example.notificationms.dto.request.NotificationRequestDto;

import java.util.UUID;

public interface NotificationService {
    void sendNotification(NotificationRequestDto notificationRequestDto, UUID userId, String userEmail);
}
