package com.example.notificationms.service.impl;

import com.example.notificationms.dto.request.NotificationRequestDto;
import com.example.notificationms.model.Notification;
import com.example.notificationms.repository.NotificationRepository;
import com.example.notificationms.service.NotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class NotificationServiceImpl implements NotificationService {
    private final NotificationRepository notificationRepository;

    @Override
    public void sendNotification(NotificationRequestDto notificationRequestDto, UUID userId, String userEmail) {
        notificationRepository.save(Notification.builder()
                .userId(userId)
                .message(notificationRequestDto.message())
                .userEmail(userEmail)
                .orderId(notificationRequestDto.orderId())
                .build());
    }

    @KafkaListener(topics = "${kafka.topic}")
    public void consume(ConsumerRecord<String, NotificationRequestDto> payload) {
        notificationRepository.save(Notification.builder()
                .userId(payload.value().userId())
                .message(payload.value().message())
                .userEmail(payload.value().userEmail())
                .orderId(payload.value().orderId())
                .build());
    }
}
